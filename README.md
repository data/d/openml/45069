# OpenML dataset: Diabetes130US

https://www.openml.org/d/45069

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The dataset represents 10 years (1999-2008) of clinical care at 130 US hospitals and integrated delivery networks. It includes over 50 features representing patient and hospital outcomes. Information was extracted from the database for encounters that satisfied the following criteria:(1) It is an inpatient encounter (a hospital admission).(2) It is a diabetic encounter, that is, one during which any kind of diabetes was entered to the system as a diagnosis.(3) The length of stay was at least 1 day and at most 14 days.(4) Laboratory tests were performed during the encounter.(5) Medications were administered during the encounter.The data contains such attributes as patient number, race, gender, age, admission type, time in hospital, medical specialty of admitting physician, number of lab test performed, HbA1c test result, diagnosis, number of medication, diabetic medications, number of outpatient, inpatient, and emergency visits in the year before the hospitalization, etc.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45069) of an [OpenML dataset](https://www.openml.org/d/45069). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45069/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45069/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45069/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

